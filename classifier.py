# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

from sklearn.neural_network import MLPRegressor
import data

def load_data():
    '''Loads the processed data and reshapes it so that
    it can be used in the sklearn MLPRegressor'''
    
    train_in = data.train_in.reshape(249,38*3)
    train_out = data.train_out 
    test_in = data.test_in.reshape(49,38*3)
    test_out = data.test_out

    return (train_in, train_out, test_in, test_out)


def run(iterations):
    '''Executes the Neural Network with a user provided number of iterations. 
    Outputs the percentage of results which correspond to arrival times later than
    the dispatch time. '''
    
    train_in, train_out, test_in, test_out = load_data()
    
    clf = MLPRegressor(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(5,3), max_iter = iterations)
    clf.fit(train_in, train_out)
    
    results = []
    for point in test_in:
        result = clf.predict(point.T)
        results.append(result)
        
    return results

    