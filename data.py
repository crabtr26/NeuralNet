import os
import csv
import numpy as np

cwd = os.getcwd()
filename_train = cwd + '/' + 'ResponseTimes.train.csv'
filename_test = cwd + '/' + 'ResponseTimes.test.csv'

def upload_database(filename):
    '''Takes in a csv file containing paramedic response times.
    The upload_database() function will read the .csv file, and
    create a new database object containing only the categories
    of information we are intereseted in using for our neural 
    network. Those categories are, time of dispatch call, time
    of arrival on scene, city in which the scene is located, and 
    the type of injury. One datapoint corresonds to a list containing
    these four pieces of information for a given 911 call. Each 
    datapoint is then stored in the database, which is a list of lists. '''
    
    database = []
    with open(filename, newline = '\n') as f:
        reader = csv.reader(f, delimiter = ',')

        # Grabbing the datapoints we are interested in, adding them to a list
        for row in reader:
            dispatch = row[3]
            arrival =row[5]
            city = row[9]
            injury = row[11]

            datapoint = [dispatch, arrival, city, injury]
            database.append(datapoint)  
            
    return database

def process(database):
    '''Takes in a database which is created by the upload_database function
    and processes the data so that it can be used in the sklearn MLPRegressor.
    The output of process() is two new databases, one which contains the 
    processed input feature vectors, and another which contains their corresponding
    outputs (i.e. paramedic arrival time). Each feature in the feature vectors is 
    represented as a numpy array of 38 values. If the feature was a time of day,
    the time was converted to minutes, scaled from 0-1, and inserted into the first
    position of the feature array. If the feature was a city, or injury type, that
    feature was mapped to a unique integer, and the corresponding feature array
    contained a single 1 at the location of that unique integer. For example, 
    if 'Oakland' maps to 5, then the 'Oakland' feature is represented as a numpy
    array of 37 zeros, and a 1 located at the 6th (not 5th because of weird python
    counting) position in the array.'''
    
    
    processed_input_database = []
    processed_output_database = []
    
    cities = []
    injuries = []
    
    # Creating lists of cities and injuries
    for datapoint in database:
        city = datapoint[2]
        injury = datapoint[3]
        cities.append(city)
        injuries.append(injury)
    
    # Making sure the lists of cities and injuries only contain unique members
    uniq_cities = set(cities)
    uniq_injuries = set(injuries)    
    
    # Assigning a unique number value for each city and injury
    city_dict = dict(zip(uniq_cities, list(range(len(uniq_cities)))))
    injury_dict = dict(zip(uniq_injuries, list(range(len(uniq_injuries)))))
    
    # Adding processed datapoints to new database
    for datapoint in database[1:]:
        dispatch = datapoint[0]
        arrival = datapoint[1]
        city = datapoint[2]
        injury = datapoint[3]
        
        # Converting time to a list of two digits [hours, minutes]
        temp_dispatch = list([int(s) for s in dispatch.split(':')[0:2] if s.isdigit()])
        temp_arrival = [int(s) for s in arrival.split(':')[0:2] if s.isdigit()]
        
        # Converting times again, this time to minutes scaled from 0-1
        processed_dispatch = np.zeros(38)
        processed_arrival = np.zeros(38)
        processed_dispatch_point = float((60*temp_dispatch[0] + temp_dispatch[1]) / (60*24))
        processed_arrival_point = float((60*temp_arrival[0] + temp_arrival[1]) / (60*24))
        processed_dispatch[0] = processed_dispatch_point
        processed_arrival[0] = processed_arrival_point
        
        
        # Converting city, injury to their corresponding values, scaled from 0-1
        processed_city = np.zeros(38)
        city_index = city_dict[city]
        processed_city[city_index] = 1
        processed_injury = np.zeros(38)
        injury_index = injury_dict[injury]
        processed_injury[injury_index] = 1
        
        # Storing processed data inside of separate input and output lists
        temp = np.array([processed_dispatch, processed_city, processed_injury])
        processed_input_datapoint = temp
        processed_input_database.append(processed_input_datapoint)
        processed_output_database.append(processed_arrival)
        
    return np.array(processed_input_database), processed_output_database
    
database_train = upload_database(filename_train)
train_in, train_out = process(database_train)
database_test = upload_database(filename_test)
test_in, test_out = process(database_test)